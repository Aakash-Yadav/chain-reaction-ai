
#include <stdio.h>
#include <stdbool.h>


// CONST VALUES 
short int Corner[4]=  {0,5,59,54};
short int Left[8]  =  {6, 12, 18, 24, 30, 36, 42, 48};
short int Right[8] =  {11, 17, 23, 29, 35, 41, 47, 53};
short int Upper[4] =  {1,2,3,4};
short int Down[4]  =  {55, 56, 57, 58};


typedef struct color_combinations_and_game_array_logic{
    
    int index_value;
    int index_color;
    bool game_continue;

}color_array;

int change_in_color(int n,int n1){
    return n1;
}

int search(short int arr[], int n, int x){
    int i,c=0;
    for (i = 0; i < n; i++)
        if (arr[i] == x){
            c++;
            break;
        }
    return c;
}

int search_2(color_array *arr){
    int i,c=0;
    for (i = 0; i <60; i++)
        if ((arr+i)->game_continue == 1){
            c++;break;
        }else{
            continue;
        }
    return c;
}

/// @brief ///
/// @param RETURN_STRUCT 
/// @return RETURN_STRUCT Pointer //

color_array *Odd_Man_Out(color_array *RETURN_STRUCT ){
    // int i=0; 
    for(int i=0;i<60;i++){
//For The Corner Odd_Man;
    if(search(Corner,4,i) && ((RETURN_STRUCT+i)->index_value)>1){
        (RETURN_STRUCT+i)->game_continue = 1;
        // printf(" \n %d \n",*(GAME_ARRAY+i)>1);
//For The Upper and Down  Odd_Man;
    }
    if(search(Upper,4,i) || search(Down,4,i)){
        if(((RETURN_STRUCT+i)->index_value)>2){
            (RETURN_STRUCT+i)->game_continue = 1;
        }
    }
    if((((RETURN_STRUCT+i)->index_value)>4) || ((RETURN_STRUCT+i)->index_value==4)){
        (RETURN_STRUCT+i)->game_continue = 1;
    }
    if(search(Right,8,i) || search(Left,8,i)){
         if(((RETURN_STRUCT+i)->index_value)>2){
            (RETURN_STRUCT+i)->game_continue = 1;
         }
     }
    }
    return RETURN_STRUCT;
}
/// @brief ////////////
/// @param index 
/// @param Index_Value 
void add_con_values(int index,color_array  *Index_Value){
    int n1 = (Index_Value+(index))->index_color;
    (Index_Value+index)->index_color=0;
    if(!index){
        (Index_Value+(index+6))->index_value+=1;
        (Index_Value+(index+6))->index_color = change_in_color((Index_Value+(index+6))->index_color,n1);
        ///////////////////////////////////
        (Index_Value+(index+1))->index_value+=1;
        (Index_Value+(index+1))->index_color = change_in_color((Index_Value+(index+1))->index_color,n1);
        
    }else if((index+1)%60==0){
        (Index_Value+(index-6))->index_value+=1;
        
        (Index_Value+(index-6))->index_color = change_in_color((Index_Value+(index-6))->index_color,n1);
        ///////////////////////////////////////////////
         (Index_Value+(index-1))->index_value+=1;
         (Index_Value+(index-1))->index_color = change_in_color((Index_Value+(index-1))->index_color,n1);
         
    }else if(!(index-5)){
        (Index_Value+(index+6))->index_value+=1;
        (Index_Value+(index+6))->index_color = change_in_color((Index_Value+(index+6))->index_color,n1);
        ////////////////////////////////////////////////
        (Index_Value+(index-1))->index_value+=1;
        (Index_Value+(index-1))->index_color = change_in_color((Index_Value+(index-1))->index_color,n1);
    }else{
        // printf("YESSS");
        (Index_Value+(index+1))->index_value+=1;
        (Index_Value+(index+1))->index_color = change_in_color((Index_Value+(index+1))->index_color,n1);
        //////////////////////////////////////////////////
        (Index_Value+(index-6))->index_value+=1;
        (Index_Value+(index-6))->index_color = change_in_color((Index_Value+(index-6))->index_color,n1);
    }

}

/// @brief //////
/// @param index 
/// @param Index_Value 
void add_up_down(int index,color_array *Index_Value){
    printf("\n %d \n",index);
    int n1 = (Index_Value+(index))->index_color;
    (Index_Value+index)->index_color=0;
    (Index_Value+(index+1))->index_value+=1;
    (Index_Value+(index-1))->index_value+=1;
    ////////////////////////////////////////////////
    (Index_Value+(index+1))->index_color=change_in_color((Index_Value+(index+1))->index_color,n1);
    (Index_Value+(index-1))->index_color=change_in_color((Index_Value+(index-1))->index_color,n1);
    if(index<6){
        // n[index+6]+=1;n[index-1]+=1;n[index+1]+=1;
        (Index_Value+(index+6))->index_value+=1;
        (Index_Value+(index+6))->index_color=change_in_color((Index_Value+(index+6))->index_color,n1);
    }
    else{
// n[index-6]+=1;n[index-1]+=1;n[index+1]+=1;
    (Index_Value+(index-6))->index_value+=1;
    (Index_Value+(index-6))->index_color=change_in_color((Index_Value+(index-6))->index_color,n1);
    }
    
}

/// @brief /////
/// @param index 
/// @param Index_Value 
void add_left_right(int index,color_array *Index_Value){
    // if(index%6==0):
    //     n[index+6]+=1;n[index-6]+=1;n[index+1]+=1;
    int n1 = (Index_Value+(index))->index_color;
    (Index_Value+index)->index_color=0;
    (Index_Value+(index+6))->index_value+=1;
    (Index_Value+(index-6))->index_value+=1;
    (Index_Value+(index-6))->index_color=change_in_color((Index_Value+(index-6))->index_color,n1);
    (Index_Value+(index+6))->index_color=change_in_color((Index_Value+(index+6))->index_color,n1);
    /////////////////////////////
    if(index%6==0){
        (Index_Value+(index+1))->index_value+=1;
        (Index_Value+(index+1))->index_color=change_in_color((Index_Value+(index+1))->index_color,n1);
    }else{
        (Index_Value+(index-1))->index_value+=1;
        (Index_Value+(index-1))->index_color=change_in_color((Index_Value+(index-1))->index_color,n1);
    }
}

void add_num(int index, color_array *n){
    (n+index)->index_value*=0;
    int n1 = (n+(index))->index_color;
    if(search(Corner,4,index)){
        add_con_values(index,n);
    }else if(search(Left,8,index) || (search(Right,8,index))){
        add_left_right(index,n);
    }else if(search(Upper,4,index) || (search(Down,4,index))){
        add_up_down(index,n);
    }else{
    (n+(index+1))->index_value+=1;
    (n+(index-1))->index_value+=1;
    ////////////////////////////////////////////////
    (n+(index+1))->index_color=change_in_color((n+(index+1))->index_color,n1);
    (n+(index-1))->index_color=change_in_color((n+(index-1))->index_color,n1);
    ////////////////////////////////////////////////
    (n+(index+6))->index_value+=1;
    (n+(index-6))->index_value+=1;
    ////////////////////////////////////////////////
    (n+(index-6))->index_color=change_in_color((n+(index-6))->index_color,n1);
    (n+(index+6))->index_color=change_in_color((n+(index+6))->index_color,n1);
    }

}

float probability(color_array *CHECK){

    int Null=0,Green=0,RED=0;
    int i=0;
    while(i!=60){
        if((CHECK+i)->index_color==1){
            RED++;
        }
        if((CHECK+i)->index_color==2){
            Green++;
        }
        i++;
    }
    float G_P = ((float)(Green)/60.0)*100;
    float R_P = ((float)(RED)/60.0)*100;

    // printf("\n%f\n",G_P-R_P);

    // printf("GREEN = %f, RED = %f",G_P,R_P);
    return G_P-R_P;
}

float add_to_numpy(color_array *Struct_value_1,int ADD){
    color_array Struct_value_2[60];
    color_array *Struct_value = &Struct_value_2[0];
    int j=0;
    while(j!=60){
    Struct_value_2[j].index_color = (Struct_value_1+j)->index_color;
    Struct_value_2[j].index_value = (Struct_value_1+j)->index_value;
    Struct_value_2[j].game_continue = (Struct_value_1+j)->game_continue;
    j++;
    };
    Struct_value_2[ADD].index_value+=1;

    Odd_Man_Out(Struct_value);
    int True = search_2(Struct_value);
    int i = 0;
    while(True){
        for(int i=0;i<60;i++){
            if((Struct_value+i)->game_continue){
                add_num(i,Struct_value);
                (Struct_value+i)->game_continue*=0;
            }
        }
        i++;

        Odd_Man_Out(Struct_value);
        True = search_2(Struct_value);
    }
    j = 0;
    i = 0;
    int k = 0;
    while(k!=60){
         if(k%6==0){
            printf("\n");
        }
        printf("%d ",Struct_value_2[k].index_value);
        k++;
    };
    // printf("\n im c");
    printf("\n");
    float PR = probability(Struct_value);
    return PR;
}

bool win_losse(color_array *Struct_value) {
    short int i=0,G_win=0,R_win=0;
    while(i!=60){
        if(G_win>2 && R_win>2){
            break;
        }
        if((Struct_value+i)->index_color==2){
                G_win++;
        }
        if((Struct_value+i)->index_color==1){
                R_win++;
        }
        i++;
    }
    
    return (G_win>1 && R_win==0)||(G_win==0 && R_win>1);
}


/// @brief /
/// @param PYTHON_NUMPY_ARRAY_POINTER 
/// @return 

int Print_game_array(int *PYTHON_NUMPY_ARRAY_POINTER){
    int i = 0;
    while(i<(10*6)) {
        if(i%6==0){
            printf("\n");
        }
        printf("%d ",*(PYTHON_NUMPY_ARRAY_POINTER+i));
        i++;
    };
    printf("\n");
    return 0;
}

/// @brief //
/// @return 
color_array *game_solver(int game_1d_adday[],color_array *game_screen_index){

    // color_array game_screen_index[10*6];
    // int odd_man[60];
    int i=0;

    while(i!=(10*6)){
        if(game_1d_adday[i]>=5){
            game_screen_index[i].index_value = game_1d_adday[i]-4;
            game_screen_index[i].index_color = 1;
            game_1d_adday[i] -=4;
        }else{
            game_screen_index[i].index_value = game_1d_adday[i];
            if(game_1d_adday[i]==0){
                game_screen_index[i].index_color = 0;
            }else if(game_screen_index[i].index_value<5){
                 game_screen_index[i].index_color = 2;
            }
        }
        game_screen_index[i].game_continue = 0;
        // odd_man[i] = 0;
        i++;
    }
    return game_screen_index;
}
 
