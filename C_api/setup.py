from setuptools import setup
from Cython.Build import cythonize
import numpy

setup(
    ext_modules = cythonize("main.pyx"),
    include_dirs=[numpy.get_include()]
)

from os import listdir,system
for i in listdir('.'):
    if i == 'main.c' or i== 'build':
        system('rm -rf %s'%(i))
