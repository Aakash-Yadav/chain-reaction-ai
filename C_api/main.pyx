cimport numpy
from numpy import array,zeros;
from PIL import Image

cdef crop_the_image_in_game_screen(str path):
    cdef numpy.ndarray image = array(Image.open(path))[133:730,525:841];
    return image;

cdef dot_analyse_value(int n):
  if(n>530 and n<580):
    return 1;
  elif(n>830 and n<890):
    return 2;
  elif (n>1020):
    return 3;
  return 0;

cdef coloured_counters_of_dots(numpy.ndarray dot_image_array):
    cdef int black = 0;
    cdef int r=0,b=0,g=0,j=0;
    cdef numpy.ndarray x;
    cdef list new_array = [];
    cdef int arrays = 0;
    cdef int green=0,red=0;
    for arrays in range(len(dot_image_array)):  
        new_array = [list(x[:3]) for x in dot_image_array[arrays]]
        for  i in (new_array):
            r,b,g = [int(j) for j in i]
            if(r+b+g)==0 or (r+b+g)<35:
                pass;
            else:
                black+=1;
            if (r,b,g)==(2,86,86) or (r,b,g)==(1,59,59):
                green+=1;
            if (r,b,g)==(251,0,0) or (r,b,g)==(181,0,0):
                red+=1;
    return array([dot_analyse_value(black),green,red]);

cdef convert_game_screen_to_2d_numpy_array(str path):
    cdef list loop_index = [(5,50),(55,100),(110,155),(160,210),(215,260),(265,-10)] 
    cdef numpy.ndarray crop_image = crop_the_image_in_game_screen(path);
    cdef int start = 0
    cdef int end = 58;
    cdef numpy.ndarray  game_screen_to_array = zeros(10*6, dtype="i4");
    cdef loop_value = 0;
    cdef int i=0,j=0;
    cdef numpy.ndarray ratio;

    while(i!=10):
        for j in range(len(loop_index)):
            ratio = coloured_counters_of_dots(crop_image[start:end,:][:58,loop_index[j][0]:loop_index[j][-1]]);
            if ratio[1]:
                game_screen_to_array[loop_value]=(ratio[0]);
            if ratio[2]:
                game_screen_to_array[loop_value]=(ratio[0]+4);
            else:
                game_screen_to_array[loop_value]=(ratio[0]);
            loop_value+=1;
        if(i>5):
            end+=61;
        else:
            end+=59;
        start+=60;
        i+=1;
    return (game_screen_to_array)

cpdef return_image_to_2d_numpy_array(str path):
    return convert_game_screen_to_2d_numpy_array( path);